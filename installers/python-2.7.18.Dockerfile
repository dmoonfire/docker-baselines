ENV PYENV_ROOT=/root/.pyenv
ENV PATH=$PATH:$PYENV_ROOT/bin
RUN if [ ! -d $HOME/.pyenv ]; then \
      git clone https://github.com/pyenv/pyenv.git $HOME/.pyenv; \
      echo '\n\
          eval "$(pyenv init -)"\n\
      ' >> $HOME/.bashrc; \
    fi
RUN pyenv install 2.7.18 && pyenv global 2.7.18
