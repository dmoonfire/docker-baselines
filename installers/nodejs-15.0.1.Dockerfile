RUN wget https://nodejs.org/download/release/v15.0.1/node-v15.0.1-linux-x64.tar.gz \
    && mkdir -p $HOME/nodejs \
    && tar xzf node-v15.0.1-linux-x64.tar.gz --strip-components=1 -C $HOME/nodejs \
    && rm -f node-v15.0.1-linux-x64.tar.gz
ENV PATH=$PATH:/root/nodejs/bin
