RUN apt-get update \
    && DEBIAN_FRONTEND="noninteractive" \
      TZ="UTC" \
      apt-get install -y \
        build-essential \
        curl \
        git \
        libbz2-dev \
        libffi-dev \
        liblzma-dev \
        libncurses5-dev \
        libncursesw5-dev \
        libreadline-dev \
        libsqlite3-dev \
        libssl-dev \
        llvm \
        openssh-client \
        python-openssl \
        rsync \
        tk-dev \
        wget \
        xz-utils \
        zlib1g-dev \
    && rm -rf /var/lib/apt/lists/*
RUN echo '. $HOME/.profile' > $HOME/.bash_profile
