RUN wget https://download.visualstudio.microsoft.com/download/pr/fdd9ecec-56b4-40f4-b762-d7efe24fc3cd/ffef51844c92afa6714528e10609a30f/dotnet-sdk-3.1.403-linux-x64.tar.gz \
    && mkdir -p $HOME/dotnet \
    && tar xzf dotnet-sdk-3.1.403-linux-x64.tar.gz -C $HOME/dotnet \
    && rm -f dotnet-sdk-3.1.403-linux-x64.tar.gz
ENV DOTNET_CLI_TELEMETRY_OPTOUT=0
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1
ENV DOTNET_ROOT=/root/dotnet
ENV PATH=$PATH:/root/dotnet
