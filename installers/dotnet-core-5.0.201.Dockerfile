RUN wget https://download.visualstudio.microsoft.com/download/pr/73a9cb2a-1acd-4d20-b864-d12797ca3d40/075dbe1dc3bba4aa85ca420167b861b6/dotnet-sdk-5.0.201-linux-x64.tar.gz \
    && mkdir -p $HOME/dotnet \
    && tar xzf dotnet-sdk-5.0.201-linux-x64.tar.gz -C $HOME/dotnet \
    && rm -f dotnet-sdk-5.0.201-linux-x64.tar.gz
ENV DOTNET_CLI_TELEMETRY_OPTOUT=0
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1
ENV DOTNET_ROOT=/root/dotnet
ENV PATH=$PATH:/root/dotnet
