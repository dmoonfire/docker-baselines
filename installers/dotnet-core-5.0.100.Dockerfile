RUN wget https://download.visualstudio.microsoft.com/download/pr/820db713-c9a5-466e-b72a-16f2f5ed00e2/628aa2a75f6aa270e77f4a83b3742fb8/dotnet-sdk-5.0.100-linux-x64.tar.gz \
    && mkdir -p $HOME/dotnet \
    && tar xzf dotnet-sdk-5.0.100-linux-x64.tar.gz -C $HOME/dotnet \
    && rm -f dotnet-sdk-5.0.100-linux-x64.tar.gz
ENV DOTNET_CLI_TELEMETRY_OPTOUT=0
ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1
ENV DOTNET_ROOT=/root/dotnet
ENV PATH=$PATH:/root/dotnet
