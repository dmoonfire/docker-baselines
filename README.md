Docker Baselines
================

A set of Docker images that include baseline tools installed. This was mainly created because sometimes a project requires a variety of different libraries and it is nice to just have a single image that contains all of them installed.

This is also intended to work with [asdf](https://github.com/asdf-vm/asdf) and the resulting `.tool-versions` file that is created.

## Useful Scripts

To clear out all the local images (rather dangerous):

```
docker image ls -q | xargs docker image rm
```
