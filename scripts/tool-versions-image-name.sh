#!/bin/bash

# Figure out the base image.
BASE_IMAGE=ubuntu-latest

echo -n $BASE_IMAGE--

# Grab the tool versions, replace the spaces with dashes, then combine
# each line together using `--` to separate each program. This is
# sorted so it will always create a consistent ordering.
cat .tool-versions \
  | perl -ne 's@python (.*) (.*)@python $1--python $2@;print' \
  | sed 's@ @-@g' \
  | sort \
  | (readarray -t ARRAY;IFS='=';echo "${ARRAY[*]}") \
  | sed 's@=@--@g'
