#!/bin/bash

# Move into the root directory.
cd $(dirname $0)
cd ..

# Go through the files.
for i in $(cat images.txt)
do
  ./scripts/make-dockerfiles.sh $i
done
