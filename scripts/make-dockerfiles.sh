#!/bin/bash

# USAGE: $0 image-name

# Make sure we have the right parameters.
if [ "x$1" = "x" ]
then
  echo "USAGE: $0 image-name--package-name"
  exit 1
fi

# Pull in our environment variables
. $(dirname $0)/../env

# Split apart the string into its components.
IFS='=' read -r -a array <<< $(echo $1 | sed 's@--@=@g')

echo "CREATING: $1"

# Make sure we have the build directory.
mkdir -p build

# Use the first one to figure out the base image. We will be building images of
# this one in the same order to avoid duplicating ourselves too much.
PREFIX=${array[0]}
FILE="build/000-$PREFIX.Dockerfile"
IMG=$(echo $PREFIX | sed 's@-@:@')
echo "FROM $IMG" > $FILE
cat installers/common.Dockerfile >> $FILE

# For the rest of the pages, we'll build a docker image from each one starting
# from the left and going on. To avoid duplicating a lot of effort, we're going
# to make a package from each package (and its dependencies) and then write them
# in a file that will build everything in a neat order once we're done.
INDEX=0

for element in "${array[@]: 1}"
do
  # Make some noise.
  echo "PROCESSING: $element FROM $PREFIX"

  # Increment the index
  INDEX=$(expr $INDEX + 1)
  FILE=build/$(printf '%03d-%s--%s.Dockerfile' $INDEX $PREFIX $element)

  # Create the Dockerfile for this installer.
  if [ -f "installers/$element.Dockerfile" ]
  then
    echo "FROM $REGISTRY/$PREFIX" > $FILE
    echo >> $FILE
    echo "#" >> $FILE
    echo "# $element" >> $FILE
    echo "#" >> $FILE
    echo >> $FILE
    cat installers/$element.Dockerfile >> $FILE
  else
    echo "Cannot find installer for $element"
    exit 1
  fi

  # Update the prefix.
  PREFIX="$PREFIX--$element"
done
