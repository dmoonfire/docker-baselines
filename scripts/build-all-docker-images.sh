#!/bin/bash

# USAGE: $0

# Pull in our environment variables
. $(dirname $0)/../env

# Go through and build each one in order.
for i in $(ls build/???-*.Dockerfile | sort)
do
  TAG=$REGISTRY/$(basename $i .Dockerfile | cut -c 5-)
  echo "BUILDING $TAG"
  docker build --tag $TAG -f $i .
done
